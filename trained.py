import tensorflow as tf
import numpy as np

from model import Model
from batches import Batches, DATA_SET_COUNT

BATCH_SIZE = 1024
DATA_COUNT = 10000

batches = Batches(BATCH_SIZE, DATA_COUNT)
# model = Model(DATA_SET_COUNT)

with tf.Graph().as_default(), tf.Session(config=tf.ConfigProto(
    log_device_placement=False,
    device_count={'GPU': 0}
)) as session:
    model = Model(DATA_SET_COUNT, True)

    saver = tf.train.Saver()
    # saver = tf.train.import_meta_graph(
    #     "./pretrained-models/classifier_1526489706/model.ckpt-20.meta"
    #     # "./output/classifier_1526487748/model.ckpt-0.meta"
    # )

    saver.restore(
        session,
        "./pretrained-models/classifier_1526489706/model.ckpt-270"
        # "./pretrained-models/classifier_noconvolutions_1526500747/model.ckpt-990"
        # "./output/classifier_1526487748/model.ckpt-0"
    )

    print("Model loaded.")

    accum_guesses = np.zeros([DATA_SET_COUNT, DATA_SET_COUNT], dtype=int)

    while batches.has_batches_testing():
        X, Y = batches.get_next_batch_testing()

        # X, Y = batches.get_test_data()
        # X = X[:1000]
        # Y = Y[:1000]

        prediction = session.run(model.nn_model, feed_dict={
            model.inputs: X, model.expected: Y})

        idx_ground_truths = np.argmax(Y, 1)
        idx_predictions = np.argmax(prediction, 1)

        for i in range(0, len(idx_ground_truths)):
            x = idx_ground_truths[i]
            y = idx_predictions[i]
            accum_guesses[x, y] += 1

        # accum_guesses[idx_ground_truths, idx_predictions] += 1

        accuracy = model.get_accuracy(Y, prediction)

        print("Accuracy: %f%%" % (accuracy * 100))

    print(accum_guesses)

    session.close()
