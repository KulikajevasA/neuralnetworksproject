from matplotlib import pyplot as plt
import numpy as np
import scipy.misc
from model import Model
from batches import DATA_SET_COUNT, SAMPLES
import tensorflow as tf

with tf.Graph().as_default(), tf.Session(config=tf.ConfigProto(
    log_device_placement=False,
    device_count={'GPU': 0}
)) as session:
    model = Model(DATA_SET_COUNT, True)

    saver = tf.train.Saver()
    # saver = tf.train.import_meta_graph(
    #     "./pretrained-models/classifier_1526489706/model.ckpt-20.meta"
    #     # "./output/classifier_1526487748/model.ckpt-0.meta"
    # )

    saver.restore(
        session,
        "./pretrained-models/classifier_1526489706/model.ckpt-270"
        # "./output/classifier_1526487748/model.ckpt-0"
    )

    print("Model loaded.")

    class LineBuilder:
        def create_line(self):
            line, = self.__axes.plot([], [], color="black")
            line.set_linewidth(5)

            self.__xs = list(line.get_xdata())
            self.__ys = list(line.get_ydata())

            self.cidpress = line.figure.canvas.mpl_connect(
                "button_press_event", self.on_press)
            self.cidrelease = line.figure.canvas.mpl_connect(
                "button_release_event", self.on_release)
            self.cidmotion = line.figure.canvas.mpl_connect(
                "motion_notify_event", self.on_motion)

            self.__line = line

            return line

        def __init__(self, fig, axes):
            self.__axes = axes
            self.create_line()
            self.__figure = fig

            self.__is_down = False

        def on_press(self, event):
            if event.inaxes != self.__line.axes:
                return
            # self.xs.append(event.xdata)
            # self.ys.append(event.ydata)
            # self.line.set_data(self.xs, self.ys)
            # self.line.figure.canvas.draw()

            self.__is_down = True

            # print("click", event)

        def on_release(self, event):

            if event.inaxes != self.__line.axes:
                return

            self.__is_down = False
            self.create_line()
            # print("release", event)

            self.guess()

        def on_motion(self, event):

            if event.inaxes != self.__line.axes or self.__is_down != True:
                return

            self.__xs.append(event.xdata)
            self.__ys.append(event.ydata)
            self.__line.set_data(self.__xs, self.__ys)
            self.__line.figure.canvas.draw()

        def guess(self):
            data = self.transform_data() / 255

            prediction = session.run(model.nn_model, feed_dict={
                model.inputs: [np.reshape(data, -1)]})

            max = np.argmax(prediction)

            print(SAMPLES[max], prediction)

            # plt.figure("sub")
            # plt.imshow(data, cmap="gray")
            # plt.show()

        def transform_data(self):
            fig = self.__figure

            plt.axis("off")
            self.__axes.set_title("")
            fig.canvas.draw()

            data = np.fromstring(fig.canvas.tostring_rgb(),
                                 dtype=np.uint8, sep="")
            data = data[::1]
            data =                 data.reshape(fig.canvas.get_width_height()[::-1] + (3,))

            data = scipy.misc.imresize(data, (28, 28), interp="lanczos")
            data = np.reshape(data, [-1])
            data = data[::3]
            data = np.reshape(data, [28, 28])

            plt.figure("main")
            self.__axes.set_title("draw")
            plt.axis("on")
            # fig.canvas.draw()
            # plt.show()

            return data

    fig = plt.figure("main")

    # DPI = fig.get_dpi()
    # fig.set_size_inches(28.0/float(DPI),28.0/float(DPI))

    ax = fig.add_subplot(111)
    ax.set_title("draw")
    linebuilder = LineBuilder(fig, ax)

    plt.show()
