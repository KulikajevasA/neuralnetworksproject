import tensorflow as tf
import numpy as np
import time
from math import sqrt
from batches import Batches, DATA_SET_COUNT
from model import Model, Training

FILE_NAME = "classifier_%i" % (round(time.time()))
EPOCHS_AUTOENCODER = 200
EPOCHS = 1000
SHOW_PLOTS = False
TRAINING_RATE = 0.1
TRAINING_MOMENTUM = 0.05
BATCH_SIZE = 1024
DATA_COUNT = 10000


batches = Batches(BATCH_SIZE, DATA_COUNT)
model = Model(DATA_SET_COUNT, False)
training = Training(model, TRAINING_RATE, TRAINING_MOMENTUM)

# plt.ion()


saver = tf.train.Saver()

with tf.Session(config=tf.ConfigProto(
    log_device_placement=False,
    device_count={'GPU': 0}
)) as session:
    writer = tf.summary.FileWriter(
        "./logs/%s" % (FILE_NAME), session.graph)

    init = tf.global_variables_initializer()
    session.run(init)

    for i in range(0, EPOCHS):
        while batches.has_batches():
            X, Y = batches.get_next_batch()

            session.run(training.train_step, feed_dict={
                model.inputs: X, model.expected: Y})

        batches.reset()

        if i % 10 == 0:
            X, Y = batches.get_test_data()
            X1, Y1 = batches.get_next_batch()
            batches.reset()

            X = X[:1000]
            Y = Y[:1000]

            [error, prediction] = session.run([training.loss, model.nn_model], feed_dict={
                model.inputs: X, model.expected: Y})
            [error1, prediction1] = session.run([training.loss, model.nn_model], feed_dict={
                model.inputs: X1, model.expected: Y1})

            accuracy = model.get_accuracy(Y, prediction)
            accuracy1 = model.get_accuracy(Y1, prediction1)

            print("Epoch: %i \n\tError: %f%% | %f%%" %
                  (i, accuracy * 100, accuracy1 * 100))

            summary = tf.Summary()
            summary.value.add(tag="loss_classifier_testing",
                              simple_value=error)
            summary.value.add(tag="loss_classifier_training",
                              simple_value=error1)
            writer.add_summary(summary, i)

            saver.save(session, "./output/%s/model.ckpt" %
                       FILE_NAME, global_step=i)

    writer.close()
    session.close()
