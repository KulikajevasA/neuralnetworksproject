import numpy as np

SHOW_PLOTS = True

if SHOW_PLOTS:
    import matplotlib.pyplot as plt

SAMPLES = [
    "axe",
    "banana",
    "bat",
    "bird",
    "cake",
    "carrot",
    "cloud",
    "dog",
    "hexagon",
    "lollipop"
]
DATA_SET_COUNT = len(SAMPLES)


class DataSet:
    training = None
    testing = None
    name = None

    def __init__(self, name, index, data_count):
        self.name = name

        data = np.load("./dataset/samples_%s.npy" % name)[:data_count]

        n_total_data = len(data)

        n_training_data = round(n_total_data * 0.8)
        # n_testing_data = n_total_data - n_training_data

        self.training = data[:n_training_data] / 255
        self.testing = data[n_training_data:] / 255
        self.answer = np.zeros(DATA_SET_COUNT)
        self.answer[index] = 1.0

        print("Loaded %s (%i|%i)" %
              (name, len(self.training), len(self.testing)))


class Batches:
    __training = np.empty([0, 784])
    __testing = np.empty([0, 784])
    __answers_training = np.empty([0, DATA_SET_COUNT])
    __answers_testing = np.empty([0, DATA_SET_COUNT])

    __batch_size = None
    __offset = 0
    __offset_testing = 0
    __has_batches = False
    __has_batches_testing = False

    def unison_shuffled_copies(self, a, b):
        # return a, b
        assert len(a) == len(b)
        p = np.random.permutation(len(a))
        return a[p], b[p]

    def __init__(self, batch_size, data_count, only_testing=False):
        training = np.empty([0, 784])
        testing = np.empty([0, 784])
        answers_training = np.empty([0, DATA_SET_COUNT])
        answers_testing = np.empty([0, DATA_SET_COUNT])

        for i in range(0, DATA_SET_COUNT):
            dataset = DataSet(SAMPLES[i], i, data_count)

            training = np.concatenate([training, dataset.training])
            testing = np.concatenate([testing, dataset.testing])

            answers_training = np.concatenate([answers_training, np.full(
                [len(dataset.training), DATA_SET_COUNT], dataset.answer)])
            answers_testing = np.concatenate([answers_testing, np.full(
                [len(dataset.testing), DATA_SET_COUNT], dataset.answer)])

            if SHOW_PLOTS:
                data = np.reshape(dataset.training[5], [28, 28])
                plt.figure(SAMPLES[i])
                # plt.subplot(121)
                plt.imshow(data, cmap="gray")
                # plt.subplot(122)
                # plt.hist(data.ravel(), bins=5, range=(0.0, 1.0), fc='k', ec='k')

        if not only_testing:
            self.__training, self.__answers_training = self.unison_shuffled_copies(
                training, answers_training)
        self.__testing, self.__answers_testing = self.unison_shuffled_copies(
            testing, answers_testing)

        self.__batch_size = batch_size
        self.__has_batches = len(training) > 0
        self.__has_batches_testing = len(testing) > 0

        if SHOW_PLOTS:
            plt.show()

    def reset(self):
        self.__offset = 0
        self.__has_batches = len(self.__training) > 0

    def reset_testing(self):
        self.__offset_testing = 0
        self.__has_batches_testing = len(self.__testing) > 0

    def has_batches(self):
        return self.__has_batches

    def has_batches_testing(self):
        return self.__has_batches_testing

    def get_next_batch(self):
        el_first = self.__offset
        el_last = self.__offset + self.__batch_size

        elements = len(self.__training)

        inputs = self.__training[el_first:el_last]
        outputs = self.__answers_training[el_first:el_last]

        if el_last >= elements:
            el_last = 0
            self.__has_batches = False

        self.__offset = el_last

        return inputs, outputs

    def get_test_data(self):
        return self.__testing, self.__answers_testing

    def get_next_batch_testing(self):
        el_first = self.__offset_testing
        el_last = self.__offset_testing + self.__batch_size

        elements = len(self.__testing)

        inputs = self.__testing[el_first:el_last]
        outputs = self.__answers_testing[el_first:el_last]

        if el_last >= elements:
            el_last = 0
            self.__has_batches_testing = False

        self.__offset_testing = el_last

        return inputs, outputs
