import tensorflow as tf
import numpy as np


class Training:
    def __init__(self, model, training_rate, training_momentum):
        loss = tf.losses.softmax_cross_entropy(model.expected, model.nn_model)

        train_step = tf.train.MomentumOptimizer(
            training_rate, training_momentum, name="optimizer").minimize(loss)

        self.train_step = train_step
        self.loss = loss


class Model:
    def __init__(self, classess, use_convolutions):
        inputs = tf.placeholder(tf.float32, [None, 784])  # 28x28 px
        expected = tf.placeholder(tf.float32, [None, classess])

        if use_convolutions:
            input_layer = tf.reshape(inputs, [-1, 28, 28, 1])

            # Convolutional Layer #1
            conv1 = tf.layers.conv2d(
                name="conv1",
                inputs=input_layer,
                filters=32,
                kernel_size=[5, 5],
                padding="same",
                activation=tf.nn.relu,
                reuse=tf.AUTO_REUSE
            )

            # Pooling Layer #1
            pool1 = tf.layers.max_pooling2d(
                name="pool1",
                inputs=conv1,
                pool_size=[2, 2],
                strides=2
            )

            # Convolutional Layer #2 and Pooling Layer #2
            conv2 = tf.layers.conv2d(
                name="conv2",
                inputs=pool1,
                filters=64,
                kernel_size=[5, 5],
                padding="same",
                activation=tf.nn.relu,
                reuse=tf.AUTO_REUSE
            )

            pool2 = tf.layers.max_pooling2d(
                name="pool2",
                inputs=conv2,
                pool_size=[2, 2],
                strides=2
            )

            reshape_layer = tf.reshape(pool2, [-1, 7 * 7 * 64])

            dense1 = tf.layers.dense(
                reshape_layer,
                128,
                activation=tf.nn.relu,
                name="dense1",
                reuse=tf.AUTO_REUSE
            )

        else:
            dense1 = tf.layers.dense(
                inputs,
                128,
                activation=tf.nn.relu,
                name="dense1",
                reuse=tf.AUTO_REUSE
            )

        dropout1 = tf.layers.dropout(dense1, rate=0.2)

        dense2 = tf.layers.dense(
            dropout1,
            classess,
            name="dense2",
            reuse=tf.AUTO_REUSE
        )

        nn_model = tf.nn.softmax(dense2, axis=1, name="softmax")

        self.nn_model = nn_model
        self.inputs = inputs
        self.expected = expected

    def get_accuracy(self, ground_truth, prediction):
        return np.average(
            np.equal(np.argmax(ground_truth, 1), np.argmax(prediction, 1)).astype(np.float))
